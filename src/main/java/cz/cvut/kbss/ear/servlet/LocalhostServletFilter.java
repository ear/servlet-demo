package cz.cvut.kbss.ear.servlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebFilter(filterName = "Only localhost or 172.* requests", urlPatterns = {"/*"})
public class LocalhostServletFilter implements Filter {
    private static final Set<String> allowedHosts = new HashSet<>(Arrays.asList(
      "localhost",
      "127\\.0\\.0\\.1",
      ".?0:0:0:0:0:0:0:1.?",
      "172(\\.[0-9]{1,3}){3}"
    ));

    @Override
    public void init(FilterConfig filterConfig) {
        System.out.println("Initializing the filter " + getClass().getName());
    }

    private static void writePlainResponse(ServletResponse servletResponse, int statusCode, String message) throws IOException {
        HttpServletResponse httpResp = ((HttpServletResponse) servletResponse);
        httpResp.setStatus(statusCode);
        httpResp.setHeader("Content-Type", "text/plain");
        httpResp.getWriter().write(message);
        httpResp.flushBuffer();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        System.out.println("Filtering using the filter " + getClass().getName());
        final String addr = servletRequest.getRemoteAddr();
        if (allowedHosts.stream().anyMatch(addr::matches)) {
            System.out.println("- match, continuing.");
            if (servletRequest instanceof HttpServletRequest) {
                final HttpServletRequest req = (HttpServletRequest) servletRequest;
                if (!"myself".equals(req.getQueryString())) {
                    filterChain.doFilter(servletRequest, servletResponse);
                } else {
                    System.out.println("No hello to myself.");
                    throw new RuntimeException("No hello to myself.");
                }
            }
        } else {
            System.out.println("Processing skipped, returning.");
            writePlainResponse(servletResponse, 400,"This location is not allowed :(");
        }
    }

    @Override
    public void destroy() {
        // Do nothing
    }
}
