# Simplistic Servlet Demo

Simple demo of annotation-configured Servlet and Filter usage.

Tests are run using Jetty. Application is packaged as WAR and can be 
deployed to an application server, such as placing the generated file in the 
"webapps" directory of Tomcat 9. If using Tomcat 10, place the WAR in the 
"webapps-javaee" directory instead.

## Repository

* [https://gitlab.fel.cvut.cz/ear/servlet-demo](https://gitlab.fel.cvut.cz/ear/servlet-demo)

## License

LGPLv3
